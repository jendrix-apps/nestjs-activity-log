# Activity Log Module

This module allows you to add to your projects the records of the activities carried out by the users of your NestJS application
<br><br>
***Activity Log Module*** It is based on:

* [TypeORM](https://typeorm.io): For entities and database model
* [NestJS TypeORM paginate](https://github.com/nestjsx/nestjs-typeorm-paginate): For data pagination with TypeORM 

## Install
```bash
npm install nestjs-activity-log
```

## Usage
import `ActivityLogModule` in AppModule or in the module where you want to use the activity log

```typescript
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ActivityLogModule } from 'nestjs-activity-log';
import { ActivityLog } from 'nestjs-activity-log';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb://localhost:27017/log_activity_db',
      synchronize: true,
      useUnifiedTopology: true,
      entities: [
        ActivityLog,
      ],
    }),
    AppConfigModule,
    ActivityLogModule,
  ],
})
export class AppModule {
}
```
 
#### Adding the entities to your TypeORM configuration

***IMPORTANT***: The module comes with the entity `ActivityLog`, you have to add the configuration `node_modules/nestjs-activity-log/**/*.entity.js`
to let typeorm scan your entities or add them to the `entities` configuration variable in TypeORM.

#### Create activity log

Then it only remains to use the service `ActivityLogService` to register any user activity that you consider. This module is designed for applications with several clients and that in turn these clients have several users who use the application.
```typescript
//Others imports
import { ActivityLogService } from 'nestjs-activity-log';

@Injectable()
export class BookService {

  constructor(private activityLogService: ActivityLogService) {
  }

  async createBook(bookDto: BookDto): Promise<Book> {
      // your logic to create the entity      
      const data = new CreateActivityLog();
      data.domain = 'TOM-BOMBADIL-LIBRARY'; // client-id
      data.action = 'CREATE-BOOK';
      data.username = 'Jhon Smith';
      data.userId = '12132';
      data.detail = 'Book created: The Lord of the Rings, The Return of the King';
      data.additionalInfo = {}; // You can indicate the information you consider necessary
      await this.activityLogService.create(data);
      // your logic to create the entity      
  }

}
```

#### Get activities log

To consult the registered activities, you just have to invoke the `getActivities` method indicating the param `domain` and retrieve the activities in reverse chronological order.
Additionally you can specify filters and indicate paging options.

```typescript

export class ActivitiesLogController {

  constructor(private activityLogService: ActivityLogService) {
  }

  @Get()
  async getAllActivityLog(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Body('domain') domain: string,
    @Body('userId') userId: string,
    @Body('action') action: string,
  ): Promise<Pagination<ActivityLog>> {
    const searchOptions: ISearchOptions = { userId, action };
    return this.activityLogService.getActivities(domain, searchOptions, { page, limit });
  }
}
```