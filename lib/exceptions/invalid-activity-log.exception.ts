import { ValidationError } from 'class-validator';

export class InvalidActivityLogException extends Error {

  constructor(private errors: ValidationError[]) {
    super();
  }

  static withErrors(errors: ValidationError[]): InvalidActivityLogException {
    return new InvalidActivityLogException(errors);
  }
}