export * from './entities';
export * from './exceptions';
export * from './inputs';
export * from './interfaces';

export * from './activity-log.service';
export * from './activity-log.module';

//TODO: configurar package json para BUILD
