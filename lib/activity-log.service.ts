import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { validate } from 'class-validator';

import { ActivityLog } from './entities';
import { InvalidActivityLogException } from './exceptions';
import { IPagingOptions, ISearchOptions } from './interfaces';
import { CreateActivityLog } from './inputs';

@Injectable()
export class ActivityLogService {

  private readonly validatorOptions = { whitelist: true, validationError: { target: false, value: false } };

  constructor(@InjectRepository(ActivityLog) private activityLogRepository: Repository<ActivityLog>) {
  }

  public async create(createActivityLog: CreateActivityLog): Promise<ActivityLog> {
    const errors = await validate(createActivityLog, this.validatorOptions);
    if (errors.length > 0) throw InvalidActivityLogException.withErrors(errors);

    const newActivityLog = this.activityLogRepository.create({ ...createActivityLog, activityDate: new Date() });
    return this.activityLogRepository.save(newActivityLog);
  }

  public async getActivities(domain: string, searchOptions?: ISearchOptions, paging?: IPagingOptions): Promise<Pagination<ActivityLog>> {
    const pagingOptions = this.validatePagingOptions(paging);
    const conditions = this.getConditions(domain, searchOptions);
    return paginate<ActivityLog>(this.activityLogRepository, pagingOptions, conditions);
  }

  protected getConditions(domain: string, searchOptions?: ISearchOptions): FindManyOptions {
    const conditions = { domain: { $eq: domain } };
    if (searchOptions) {
      const { action, userId, dateFrom, dateTo } = searchOptions;
      if (action) conditions['action'] = { $eq: action };
      if (userId) conditions['userId'] = { $eq: userId };

      const newDateFrom = this.getFirstMomentOfDate(dateFrom);
      const newDateTo = this.getLastMomentOfDate(dateTo);
      if (newDateFrom && newDateTo) {
        conditions['activityDate'] = { $gte: newDateFrom, $lte: newDateTo };
      } else if (newDateFrom) {
        conditions['activityDate'] = { $gte: newDateFrom };
      } else if (newDateTo) {
        conditions['activityDate'] = { $lte: newDateTo };
      }
    }
    return { where: conditions };
  }

  protected validatePagingOptions(pagingOptions: IPagingOptions): IPaginationOptions {
    // eslint-disable-next-line prefer-const
    let { page, limit, route } = pagingOptions;
    if (!page || page <= 0) page = 1;
    if (!limit || limit <= 100) limit = 100;
    return { page, limit, route };
  }

  protected getLastMomentOfDate(date: Date): Date {
    if (!date) return null;
    return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 23, 59, 59, 999));
  }

  protected getFirstMomentOfDate(date: Date): Date {
    if (!date) return null;
    return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0, 0));
  }

}
