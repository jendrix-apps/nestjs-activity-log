import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { IsDate, IsNotEmpty, IsObject, IsOptional, IsString } from 'class-validator';

@Entity()
export class ActivityLog {

  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  @IsNotEmpty()
  @IsString()
  domain: string;

  @Column({ type: 'timestamp' })
  @IsNotEmpty()
  @IsDate()
  activityDate: Date;

  @Column()
  @IsNotEmpty()
  @IsString()
  action: string;

  @Column()
  @IsNotEmpty()
  @IsString()
  detail: string;

  @Column()
  @IsNotEmpty()
  @IsString()
  userId: string;

  @Column()
  @IsNotEmpty()
  @IsString()
  username: string;

  @Column()
  @IsOptional()
  @IsObject()
  additionalInfo: any;
}