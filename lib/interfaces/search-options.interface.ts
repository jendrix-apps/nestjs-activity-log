export interface ISearchOptions {

  action?: string;

  userId?: string;

  dateFrom?: Date;

  dateTo?: Date;
}