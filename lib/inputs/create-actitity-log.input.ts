import { IsNotEmpty, IsObject, IsOptional, IsString } from 'class-validator';

export class CreateActivityLog {

  @IsNotEmpty()
  @IsString()
  domain: string;

  @IsNotEmpty()
  @IsString()
  action: string;

  @IsNotEmpty()
  @IsString()
  detail: string;

  @IsNotEmpty()
  @IsString()
  userId: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsOptional()
  @IsObject()
  additionalInfo: any;
}